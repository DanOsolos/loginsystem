package Test;

import Java.SignUp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.io.InputStream;
import java.util.Scanner;

import static org.junit.Assert.assertEquals;


public class TestSignUp {



    @Test
    public void testIsDayOfBirthValidLessThanZero() {
        SignUp signUp = new SignUp();
        boolean result = signUp.isDayOfBirthValid("-55");
        assertEquals(false,result);
    }

    @Test
    public void testIsDayOfBirthValidZero() {
        SignUp signUp = new SignUp();
        boolean result = signUp.isDayOfBirthValid("0");
        assertEquals(false,result);
    }

    @Test
    public void testIsDayOfBirthValid() {
        SignUp signUp = new SignUp();
        boolean result = signUp.isDayOfBirthValid("12");
        assertEquals(true,result);
    }

    @Test
    public void testIsDayOfBirthValidMoreThan31() {
        SignUp signUp = new SignUp();
        boolean result = signUp.isDayOfBirthValid("55");
        assertEquals(false,result);
    }

}
