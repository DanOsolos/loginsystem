package Java;

import java.util.Scanner;

public class LogIn {
    public static void login() {
        Account user = new Account();
        Scanner scanner = new Scanner(System.in);
        char option;
        do {
            Menu.showMenu();
            option = scanner.next().charAt(0);
            switch (option) {
                case 'A':
                    System.out.println("Create a new user account");
                    System.out.println("\nPlease insert your username:");
                    user.userName = SignUp.newUsername();

                    System.out.println("Please insert your password:");
                    user.password = SignUp.newPassword();

                    System.out.println("Your day of birth:");
                    user.dayOfBirth = SignUp.newDayOfBirth();

                    System.out.println("What month were you born in?");
                    System.out.println("1: JANUARY\n2: FEBRUARY\n3: MARCH\n4: APRIL\n5: MAY\n6: JUNE\n7: JULY\n8: AUGUST\n9: SEPTEMBER\n10: OCTOBER\n11: NOVEMNER\n12: DECEMBER");
                    user.monthOfBirth = SignUp.newMonthOfBirth();

                    System.out.println("What is your year of birth?");
                    user.yearOfBirth = SignUp.newYearOfBirth();

                    System.out.println("User " + user.userName + " with password " + user.password+ " has been created. This user birthday is on " + user.dayOfBirth + " " + user.monthOfBirth + " " + user.yearOfBirth);

                    break;
                case 'B':
                    System.out.println("Username:");
                    String introducedUserName = LogIn.introducedUserName();

                    System.out.println("Password:");
                    String introducedPassword = LogIn.introducedPassword();

                    if (introducedUserName.equals(user.userName) && introducedPassword.equals(user.password)) {
                        System.out.println("Welcome " + user.userName + "," + " your birthday is on " + user.dayOfBirth + " " + user.monthOfBirth + " " + user.yearOfBirth);
                    } else {
                        System.out.println("User or password incorrect");
                    }
                    System.out.println("X. Exit");
                    option = scanner.next().charAt(0);
                    break;
                case 'X':
                    System.out.println("Program terminated");
                    break;
                default:
                    System.out.println("Not a valid option. Choose another option.");
            }
        } while (option != 'X');
    }
        public static String introducedUserName () {
            Scanner scanner = new Scanner(System.in);
            String introducedUserName = scanner.nextLine();
            return introducedUserName;
        }

        public static String introducedPassword () {
            Scanner scanner = new Scanner(System.in);
            String introducedPassword = scanner.nextLine();
            return introducedPassword;
        }
    }

