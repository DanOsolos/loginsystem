package Java;

import java.util.Scanner;

public class SignUp {
    public static String newUsername() {
        Scanner scanner = new Scanner(System.in);
        String userName = scanner.nextLine();
        return userName;
    }

    public static String newPassword() {
        Scanner scanner = new Scanner(System.in);
        String password = scanner.nextLine();
        return password;
    }

    public static String newDayOfBirth() {
        Scanner scanner = new Scanner(System.in);
        String dayOfBirth = scanner.nextLine();
        if (isDayOfBirthValid(dayOfBirth)) {
            return dayOfBirth;
        } else {
            System.out.println("Day of birth invalid, enter a new day of birth: ");
            return dayOfBirth;
        }
    }

    public static boolean isDayOfBirthValid(String dayOfBirth) {
        return Integer.parseInt(dayOfBirth) > 0 && Integer.parseInt(dayOfBirth) < 32;
    }

    public static String newMonthOfBirth() {
        Scanner scanner = new Scanner(System.in);
        int month = scanner.nextInt();
        String monthOfBirth = "";
        switch (month) {
            case 1:
                monthOfBirth = "JANUARY";
                break;
            case 2:
                monthOfBirth = "FEBRUARY";
                break;
            case 3:
                monthOfBirth = "MARCH";
                break;
            case 4:
                monthOfBirth = "APRIL";
                break;
            case 5:
                monthOfBirth = "MAY";
                break;
            case 6:
                monthOfBirth = "JUNE";
                break;
            case 7:
                monthOfBirth = "JULY";
                break;
            case 8:
                monthOfBirth = "AUGUST";
                break;
            case 9:
                monthOfBirth = "SEPTEMBER";
                break;
            case 10:
                monthOfBirth = "OCTOBER";
                break;
            case 11:
                monthOfBirth = "NOVEMBER";
                break;
            case 12:
                monthOfBirth = "DECEMBER";
                break;
            default:
                System.out.println("Enter a valid option");
        }
        return monthOfBirth;
    }

    public static String newYearOfBirth() {
        Scanner scanner = new Scanner(System.in);
        String yearOfBirth = scanner.nextLine();
        return yearOfBirth;
    }
}
